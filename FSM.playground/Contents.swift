import XCTest

protocol TurnstileState {
    func pass(fsm: TurnstileFSM)
    func coin(fsm: TurnstileFSM)
}

protocol TurnstileFSM: class {
    var state: TurnstileState { get set }
    func pass()
    func coin()
    func setState(state: TurnstileState)
    func alarm()
    func lock()
    func unlock()
    func thankyou()
}

extension TurnstileFSM {
    func pass() {
        state.pass(fsm: self)
    }
    func coin() {
        state.coin(fsm: self)
    }
    func setState(state: TurnstileState) {
        self.state = state
    }
}

enum OneCoinTurnstileState: TurnstileState {
    case locked
    case unlocked
    
    func coin(fsm: TurnstileFSM) {
        switch self {
        case .locked:
            fsm.setState(state: OneCoinTurnstileState.unlocked)
            fsm.unlock()
        case .unlocked:
            fsm.thankyou()
        }
    }
    
    func pass(fsm: TurnstileFSM) {
        switch self {
        case .locked:
            fsm.alarm()
        case .unlocked:
            fsm.setState(state: OneCoinTurnstileState.locked)
            fsm.lock()
        }
    }
}

class TurnstileTest: XCTestCase, TurnstileFSM {
    var state: TurnstileState = OneCoinTurnstileState.locked
    var fsm: TurnstileFSM!
    var actions: String!
    override func setUp() {
        super.setUp()
        fsm = self
        fsm.setState(state: OneCoinTurnstileState.locked)
        actions = ""
    }
    
    func assertActions(expected: String) {
        XCTAssertEqual(expected, actions)
    }
    
    func testNormalOperation() {
        coin()
        assertActions(expected: "U")
        pass()
        assertActions(expected: "UL")
    }
    
    func testForcedEntry() {
        pass()
        assertActions(expected: "A")
    }
    
    func testDoublePayment() {
        coin()
        coin()
        assertActions(expected: "UT")
    }
    
    func testManyCoins() {
        for _ in 1...5 {
            coin()
        }
        assertActions(expected: "UTTTT")
    }
    
    func testManyCoinsThenPass() {
        for _ in 1...5 {
            coin()
        }
        pass()
        assertActions(expected: "UTTTTL")
    }
    
    func alarm() {
        actions += "A"
    }
    
    func lock() {
        actions += "L"
    }
    
    func unlock() {
        actions += "U"
    }
    
    func thankyou() {
        actions += "T"
    }
}

TurnstileTest.defaultTestSuite.run()
